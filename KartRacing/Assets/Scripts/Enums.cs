public enum GameState
{
    WAITING = 0,
    START,
    RUNNING,
    FINISH,
    RANKING,
}

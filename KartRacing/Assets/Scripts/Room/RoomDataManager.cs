using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public int id;
    public string playerName;
    public bool isCPU;

    public PlayerData(int id, string playerName, bool isCPU = false)
    {
        Setup(id, playerName, isCPU);
    }
    public void Setup(int id, string playerName, bool isCPU = false)
    {
        this.id = id;
        this.playerName = playerName;
        this.isCPU = isCPU;
    }
}
[Serializable]
public class RoomData
{
    public string roomId;
    public List<PlayerData> playerDatas;
}
public class RoomDataManager : MonoBehaviour
{
    public static RoomDataManager Instance { get; private set; }
    public RoomData roomData;
    public PlayerData playerData;

    private void Start()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }
    public void Init()
    {
        roomData.roomId = PhotonNetwork.CurrentRoom.Name;
        if (roomData.playerDatas == null || roomData.playerDatas.Count == 0)
        {
            roomData.playerDatas = new List<PlayerData>();
            for (int i = 0; i < 3; i++)
            {
                PlayerData playerData = new PlayerData(i + 1, "CPU" + (roomData.playerDatas.Count + 1).ToString(), true);
                roomData.playerDatas.Add(playerData);
            }
        }
    }
    public PlayerData CurrentPlayerData()
    {
        for (int i = 0; i < roomData.playerDatas.Count; i++)
        {
            if (roomData.playerDatas[i].isCPU)
            {
                roomData.playerDatas[i].Setup(i + 1, PhotonNetwork.NickName);
                return roomData.playerDatas[i];
            }
        }
        return null;
    }

    public void SetPlayerData()
    {
        this.playerData = CurrentPlayerData();
    }
}

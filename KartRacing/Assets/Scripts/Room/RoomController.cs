using NaughtyAttributes;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomController : MonoBehaviourPunCallbacks
{
    public static RoomController Instance { get; private set; }
    [SerializeField] List<UICharacterBlock> uICharacterBlocks;

    [SerializeField] PlayerData playerData;
    [SerializeField] PhotonView ptView;

    [SerializeField, ReadOnly] bool isSycnSuccess = true;
    [SerializeField] Button backBtn;
    [SerializeField] GameObject loadingCanvas;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        backBtn.onClick.AddListener(BackToLobby);

        foreach (var block in uICharacterBlocks)
        {
            block.Setup();
        }

        RoomDataManager.Instance.Init();

        CallSyncRoomData();
        StartCoroutine(Cor_waitSyncRoomData());
    }

    IEnumerator Cor_waitSyncRoomData()
    {
        yield return new WaitUntil(() => isSycnSuccess);
        Setup();
    }
    IEnumerator Cor_waitForStart()
    {
        yield return new WaitUntil(() => CheckAllReady() && isSycnSuccess);
        StartGame();
    }
    void Setup()
    {
        //this.playerData = RoomDataManager.Instance.SetupPlayerData();
        RoomDataManager.Instance.SetPlayerData();
        this.playerData = RoomDataManager.Instance.playerData;

        ReloadUI();
        CallSyncRoomData();

        StartCoroutine(Cor_waitForStart());
    }
    void ReloadUI()
    {
        RoomData roomData = RoomDataManager.Instance.roomData;
        for (int i = 0; i < roomData.playerDatas.Count; i++)
        {
            uICharacterBlocks[i].Setup(roomData.playerDatas[i].playerName, roomData.playerDatas[i].isCPU);
            if (roomData.playerDatas[i].isCPU)
            {
                uICharacterBlocks[i].SetBtnForOther(true);
            }
            else
            {
                if (roomData.playerDatas[i].id == playerData.id)
                {
                    uICharacterBlocks[i].SetBtnForPlayer();
                }
                else
                {
                    uICharacterBlocks[i].SetBtnForOther();
                }
            }
            
        }
    }
    bool CheckAllReady()
    {
        bool isHavePlayer = false;
        foreach (var uiCharacterBlock in uICharacterBlocks)
        {
            if (uiCharacterBlock.isReady == false) return false;
            if (uiCharacterBlock.isCPU == false) isHavePlayer = true;
        }
        if (isHavePlayer == false) return false;
        return true;
    }
    public void StartGame()
    {
        loadingCanvas.SetActive(true);
        PhotonNetwork.LoadLevel("Gameplay");
    }

    [PunRPC]
    void SyncRoomData(string roomDataJson)
    {
        RoomDataManager.Instance.roomData = JsonUtility.FromJson<RoomData>(roomDataJson);
        isSycnSuccess = true;
        ReloadUI();
        Debug.Log("Sync roomData success!");
    }

    void CallSyncRoomData()
    {
        Debug.Log("Call sync");
        isSycnSuccess = false;
        string roomDataJson = JsonUtility.ToJson(RoomDataManager.Instance.roomData);
        ptView.RPC("SyncRoomData", RpcTarget.AllBuffered, roomDataJson);
    }

    public void PlayerReady(bool isReady)
    {
        ptView.RPC("SyncReadyBtn", RpcTarget.AllBuffered, playerData.id, isReady);
    }

    [PunRPC]
    void SyncReadyBtn(int id, bool isReady)
    {
        if (id != playerData.id)
        {
            uICharacterBlocks[id - 1].SetBtnForOther(isReady);
        }
    }

    void BackToLobby()
    {
        Debug.Log("Back to Lobby...");
        playerData = new PlayerData(this.playerData.id, "CPU" + this.playerData.id.ToString(), true);
        RoomDataManager.Instance.roomData.playerDatas[this.playerData.id] = playerData;
        CallSyncRoomData();
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        Debug.Log("Left room");
        PhotonNetwork.LoadLevel("Lobby");
    }

    //public override void OnPlayerLeftRoom(Player otherPlayer)
    //{
    //    Debug.Log("Player left room with id: " + otherPlayer.ActorNumber + " ; and name: " + otherPlayer.NickName);
    //}
}

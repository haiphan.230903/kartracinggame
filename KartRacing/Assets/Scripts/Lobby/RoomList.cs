using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.Demo.Cockpit;
using NaughtyAttributes;

public class RoomList : MonoBehaviourPunCallbacks
{
    [SerializeField] Transform content;
    [SerializeField] UIRoom uiRoomPrefab;
    [SerializeField] List<UIRoom> uiRooms;

    [Button]
    public void Setup()
    {
        PhotonNetwork.GetCustomRoomList(TypedLobby.Default, "");
    }   
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        if (uiRooms == null)
        {
            uiRooms = new List<UIRoom>();
        }

        foreach(var uiRoom in uiRooms)
        {
            Destroy(uiRoom.gameObject);
        }
        uiRooms.Clear();

        foreach(var room in roomList)
        {
            if (room.IsOpen && room.IsVisible && room.PlayerCount>0)
            {
                UIRoom uiRoom = Instantiate(uiRoomPrefab, Vector3.zero, Quaternion.identity, content);
                uiRoom.Setup(room.Name);
                uiRooms.Add(uiRoom);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRoom : MonoBehaviour
{
    [SerializeField] TMP_Text roomText;
    [SerializeField] Button joinBtn;
    
    public void Setup(string roomName)
    {
        roomText.text = roomName;
        joinBtn.onClick.AddListener(JoinRoom);
    }
    public void JoinRoom()
    {
        LobbyController.Instance.JoinRoomInList(roomText.text);
    }
}

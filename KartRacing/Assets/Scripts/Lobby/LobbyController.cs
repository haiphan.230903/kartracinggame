using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;

public class LobbyController : MonoBehaviourPunCallbacks
{
    public static LobbyController Instance { get; private set; }
    [SerializeField] TMP_InputField nameInput, createRoomInput, joinRoomInput;
    [SerializeField] Button createBtn, joinBtn;
    [SerializeField] GameObject loadingCanvas;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        createBtn.onClick.AddListener(CreateRoom);
        joinBtn.onClick.AddListener(JoinRoom);
    }
    public void CreateRoom()
    {
        Debug.Log("Create room");
        PhotonNetwork.CreateRoom(createRoomInput.text, new RoomOptions() { MaxPlayers = 3, IsVisible = true, IsOpen = true });
        loadingCanvas.SetActive(true);
    }
    public void JoinRoom()
    {
        Debug.Log("Join room");
        SetName();
        PhotonNetwork.JoinRoom(joinRoomInput.text);
        loadingCanvas.SetActive(true);
    }
    public override void OnJoinedRoom()
    {
        SetName();
        PhotonNetwork.LoadLevel("Room");
    }
    void SetName()
    {
        PhotonNetwork.NickName = nameInput.text;
    }
    public void JoinRoomInList(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }
}

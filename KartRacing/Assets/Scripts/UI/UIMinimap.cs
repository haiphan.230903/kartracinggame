using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMinimap : MonoBehaviour
{
    [SerializeField] List<Image> playerIconRectList;
    [SerializeField] float scaleX, scaleY;
    [SerializeField] Vector2 pivotPosMinimap, pivotPosGameplay;
    public void Setup(List<KartController> kartControllerList)
    {
        for (int i = 0; i < kartControllerList.Count; i++)
        {
            Vector3 pos = kartControllerList[i].transform.position;
            pos.x -= pivotPosGameplay.x;
            pos.z -= pivotPosGameplay.y;
            playerIconRectList[i].rectTransform.anchoredPosition = pivotPosMinimap + new Vector2(pos.x * scaleX, pos.z * scaleY);
            if (kartControllerList[i].kartInfo.isMine)
            {
                playerIconRectList[i].color = Color.green;
            }
            else
            {
                playerIconRectList[i].color = Color.red;
            }
        }
    }
    public void UpdatePlayerIconPos(List<KartController> kartControllerList)
    {
        for (int i=0; i<kartControllerList.Count; i++)
        {
            Vector3 pos = kartControllerList[i].transform.position;
            pos.x -= pivotPosGameplay.x;
            pos.z -= pivotPosGameplay.y;
            playerIconRectList[i].rectTransform.anchoredPosition = pivotPosMinimap + new Vector2(pos.x * scaleX, pos.z * scaleY);
        }
    }
}

using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController Instance { get; private set; }
    [SerializeField] UIMinimap uiMinimap;
    [SerializeField] UILap uiLap;
    [SerializeField] UIRank uiRank;
    [SerializeField] UIAnnounce uiAnnounce;
    [SerializeField] UIRankingPanel uiRankingPanel;
    [SerializeField,ReadOnly] bool isReverse;
    [SerializeField, ReadOnly] int lap;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        SetupMinimap();
    }
    private void Update()
    {
        UpdateMinimap();
    }

    public void UpdateLap(int lap, bool isAnnounce = true)
    {
        uiLap.UpdateLap(lap, GameController.Instance.gameplayData.maxLap);
        if (this.lap < lap)
        {
            this.lap = lap;
            if (isAnnounce)
            {
                AnnounceLap(lap);
            }
        }
        if (lap > GameController.Instance.gameplayData.maxLap)
        {
            Goal();
        }
    }

    void SetupMinimap()
    {
        uiMinimap.Setup(GameController.Instance.gameplayData.kartControllers);
    }
    void UpdateMinimap()
    {
        uiMinimap.UpdatePlayerIconPos(GameController.Instance.gameplayData.kartControllers);
    }

    public void UpdateRank(int rank)
    {
        uiRank.UpdateRank(rank);
    }
    public void AnnounceLap(int lap)
    {
        uiAnnounce.AnnounceLap(lap, GameController.Instance.gameplayData.maxLap);
    }
    public void AnnounceReverse(bool isReverse)
    {
        if (isReverse == this.isReverse) return;
        this.isReverse = isReverse;
        if (isReverse)
        {
            uiAnnounce.AnnounceReverse();
        }
        else
        {
            uiAnnounce.EndAnnounceReverse();
        }
    }
    public void Goal()
    {
        GameController.Instance.Goal();
        uiAnnounce.Finish();
    }
    public void StartCount()
    {
        uiAnnounce.StartCount();
    }
    public void RunGame()
    {
        GameController.Instance.RunGame();
    }
    public void ShowRankingPanel()
    {
        uiRankingPanel.gameObject.SetActive(true);
        uiRankingPanel.Show();
        foreach(var kartController in GameController.Instance.gameplayData.kartControllers)
        {
            uiRankingPanel.AddRankingItem(kartController.kartInfo.name, kartController.kartInfo.finishRank);
        }
        uiRankingPanel.SortItem();
        uiRankingPanel.SetupRankItemList();
    }
    public void BackToMenu()
    {
        GameController.Instance.BackToMenu();
    }
}

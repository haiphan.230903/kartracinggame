using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIRankItem : MonoBehaviour
{
    [SerializeField] TMP_Text nameText, rankText;
    public string racerName;
    public int rank;

    public void Setup()
    {
        nameText.text = racerName;
        if (rank == 1)
        {
            rankText.text = rank.ToString() + "st";
        }
        else if (rank == 2)
        {
            rankText.text = rank.ToString() + "nd";
        }
        else if (rank == 3)
        {
            rankText.text = rank.ToString() + "rd";
        }
        else
        {
            rankText.text = rank.ToString() + "th";
        }
    }
    public void Setup(string name, int rank)
    {
        racerName = name;
        this.rank = rank;
        nameText.text = name;
        if (rank == 1)
        {
            rankText.text = rank.ToString() + "st";
        }
        else if (rank == 2)
        {
            rankText.text = rank.ToString() + "nd";
        }
        else if (rank == 3)
        {
            rankText.text = rank.ToString() + "rd";
        }
        else
        {
            rankText.text = rank.ToString() + "th";
        }
    }
}

using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterBlock : MonoBehaviour
{
    [SerializeField] TMP_Text nameText;
    [SerializeField] Button readyBtn;
    [SerializeField] TMP_Text readyText;
    [SerializeField] Image readyImage, readyBtnContain;
    [SerializeField] Color readyColor, unreadyColor;
    public bool isCPU;
    public bool isReady;
    public void Setup()
    {
        readyBtn.onClick.AddListener(ReadyBtn);
    }
    public void Setup(string name, bool isCPU)
    {
        nameText.text = name;
        this.isCPU = isCPU;
        if (isCPU)
        {
            //readyBtnContain.gameObject.SetActive(false);
            //Ready();
            SetBtnForOther(true);
        }
        else
        {
            //readyBtnContain.gameObject.SetActive(true);
            //UnReady();
            SetBtnForPlayer();
            UnReady();
        }
    }
    [Button]
    public void ReadyBtn()
    {
        if (isReady) UnReady();
        else
        {
            Ready();
            //RoomController.Instance.CheckAllReady();
        }
        RoomController.Instance.PlayerReady(isReady);
    }
    void Ready()
    {
        readyImage.color = readyColor;
        isReady = true;
        readyText.text = "READY";
    }
    void UnReady()
    {
        readyImage.color = unreadyColor;
        isReady = false;
        readyText.text = "UNREADY";
    }
    
    public void SetBtnForOther(bool isReady)
    {
        readyBtn.interactable = false;
        if (isReady) Ready();
        else UnReady();
    }
    public void SetBtnForOther()
    {
        readyBtn.interactable = false;
    }
    public void SetBtnForPlayer()
    {
        readyBtn.interactable = true;
    }
}

using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRank : MonoBehaviour
{
    [SerializeField] Image rankImage;
    [SerializeField] List<Sprite> rankSprites;
    int currentRank = 1;
    public void UpdateRank(int rank)
    {
        if (rank == currentRank) return;

        DOTween.Kill(rankImage.rectTransform);
        currentRank = rank;
        rankImage.rectTransform.DOScale(0, 0.1f).OnComplete(() =>
        {
            rankImage.sprite = rankSprites[rank - 1];
            rankImage.rectTransform.DOScale(1, 0.1f).SetEase(Ease.OutBack);
            rankImage.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
            rankImage.rectTransform.DORotate(new Vector3(0, 0, 360), 0.2f, RotateMode.FastBeyond360).SetEase(Ease.OutBack);
        });
    }
}

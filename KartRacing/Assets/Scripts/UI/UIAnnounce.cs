using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.ProBuilder.MeshOperations;
using UnityEngine.UI;

public class UIAnnounce : MonoBehaviour
{
    [Header("LAP 1 LAP 2...")]
    [SerializeField] Image announceBgImage;
    [SerializeField] TMP_Text announceText1, announceText2;
    [SerializeField] Color defaultAnnounceBgColor;

    [Header("3 2 1 GO!...")]
    [SerializeField] Image centerImage;
    [SerializeField] Sprite num1Sprite, num2Sprite, num3Sprite, goSprite, finishSprite;

    public void AnnounceLap(int lap, int maxlap)
    {
        if (lap < maxlap)
        {
            announceText1.text = "LAP " + lap.ToString();
            announceText2.text = "LAP " + lap.ToString();
        }
        else if (lap == maxlap)
        {
            announceText1.text = "FINAL LAP";
            announceText2.text = "FINAL LAP";
        }
        else
        {
            announceText1.text = "FINISH";
            announceText2.text = "FINISH";
        }
        
        //DOTween.Kill(announceBgImage.rectTransform);
        announceBgImage.rectTransform.anchoredPosition = new Vector2(-1920, announceBgImage.rectTransform.anchoredPosition.y);

        announceBgImage.color = defaultAnnounceBgColor;

        Color textColor = announceText1.color;
        textColor.a = 1;
        announceText1.color = textColor;

        textColor = announceText2.color;
        textColor.a = 1;
        announceText2.color = textColor;

        announceBgImage.rectTransform.DOAnchorPosX(0, 0.5f).OnComplete(() =>
        {
            announceBgImage.rectTransform.DOAnchorPosX(1920, 0.5f).SetDelay(1);
        });
    }
    public void AnnounceReverse()
    {
        Debug.Log("Announce reverse!");
        announceText1.text = "REVERSE!";
        announceText2.text = "REVERSE!";
        //DOTween.Kill((announceBgImage));
        announceBgImage.rectTransform.anchoredPosition = new Vector2(0, announceBgImage.rectTransform.anchoredPosition.y);
        Color transparentBgColor = defaultAnnounceBgColor;
        transparentBgColor.a = 0;
        announceBgImage.color = transparentBgColor;

        Color textColor = announceText1.color;
        textColor.a = 0;
        announceText1.color = textColor;

        textColor = announceText2.color;
        textColor.a = 0;
        announceText2.color = textColor;

        announceBgImage.DOFade(1, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        announceText1.DOFade(1, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        announceText2.DOFade(1, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }
    public void EndAnnounceReverse()
    {
        Debug.Log("Announce end reverse!");
        DOTween.Kill((announceBgImage));
        DOTween.Kill(announceText1);
        DOTween.Kill(announceText2);
        announceBgImage.DOFade(0, 0.5f).SetEase(Ease.Linear);
        announceText1.DOFade(0, 0.5f).SetEase(Ease.Linear);
        announceText2.DOFade(0, 0.5f).SetEase(Ease.Linear);
    }

    public void StartCount()
    {
        StartCoroutine(Cor_Count());
    }
    IEnumerator Cor_Count()
    {
        yield return new WaitForSeconds(1);
        AnimCenterImage(num3Sprite, 2);
        yield return new WaitForSeconds(1);
        AnimCenterImage(num2Sprite, 2);
        yield return new WaitForSeconds(1);
        AnimCenterImage(num1Sprite, 2);
        yield return new WaitForSeconds(1);
        AnimCenterImage(goSprite, 3f, 1);
        UIController.Instance.RunGame();
    }
    public void Finish()
    {
        AnimCenterImage(finishSprite, 3, 1);
    }
    void AnimCenterImage(Sprite sprite, float scale, float delay = 0.2f)
    {
        centerImage.sprite = sprite;
        centerImage.SetNativeSize();
        centerImage.color = Color.white;
        centerImage.rectTransform.localScale = Vector2.zero;
        centerImage.rectTransform.DOScale(scale, 0.2f).SetEase(Ease.OutBack).OnComplete(()=>
        {
            centerImage.DOFade(0, 0.5f).SetDelay(0.2f);
        });
    }
}

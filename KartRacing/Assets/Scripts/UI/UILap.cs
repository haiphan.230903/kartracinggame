using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UILap : MonoBehaviour
{
    [SerializeField] TMP_Text lapText1, lapText2;

    public void UpdateLap(int lap, int maxLap)
    {
        if (lap > maxLap) lap = maxLap;
        lapText1.text = "LAP " + lap.ToString() + "/" + maxLap.ToString();
        lapText2.text = "LAP " + lap.ToString() + "/" + maxLap.ToString();
    }
}

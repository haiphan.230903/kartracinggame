using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIName : MonoBehaviour
{
    [SerializeField] TMP_Text nameText, bgNameText;
    [SerializeField] Image nameLineImage, bgNameLineImage;
    bool isShowing, isMine;
    [SerializeField] float scaleDistanceRate, distanceShow;
    public void Setup(string name, bool isMine)
    {
        nameText.text = name;
        bgNameText.text = name;
        this.isMine = isMine;
        if (isMine) ShowShort();
    }
    private void Update()
    {
        if (isMine) return;

        float distance = Vector3.Distance(Camera.main.transform.position, transform.position);
        if (distance < distanceShow)
        {
            Show();
        }
        else
        {
            Hide();
        }

        transform.localScale = Vector3.one * distance * scaleDistanceRate;

        transform.rotation = Camera.main.transform.rotation;
    }
    void ShowShort()
    {
        StartCoroutine(Cor_ShowShort());
    }
    IEnumerator Cor_ShowShort()
    {
        Show();
        yield return new WaitForSeconds(2);
        Hide();
    }
    void Show()
    {
        if (isShowing) return;
        isShowing = true;
        DOTween.Kill(nameText);
        DOTween.Kill(bgNameText);
        DOTween.Kill(nameLineImage);
        DOTween.Kill(bgNameLineImage);
        nameText.DOFade(1, 0.5f);
        bgNameText.DOFade(1, 0.5f);
        nameLineImage.DOFade(1, 0.5f);
        bgNameLineImage.DOFade(1, 0.5f);
    }
    void Hide()
    {
        if (isShowing == false) return;
        isShowing = false;
        DOTween.Kill(nameText);
        DOTween.Kill(bgNameText);
        DOTween.Kill(nameLineImage);
        DOTween.Kill(bgNameLineImage);
        nameText.DOFade(0, 0.5f);
        bgNameText.DOFade(0, 0.5f);
        nameLineImage.DOFade(0, 0.5f);
        bgNameLineImage.DOFade(0, 0.5f);
    }
}

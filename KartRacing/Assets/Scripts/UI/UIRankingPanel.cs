using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRankingPanel : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;
    [SerializeField] Transform content;
    [SerializeField] GameObject rankItem;
    List<UIRankItem> uiRankItems;
    [SerializeField] Button backBtn;

    public void Show()
    {
        rectTransform.anchoredPosition = new Vector2(1500, 0);
        rectTransform.DOAnchorPosX(-532, 0.5f);
        backBtn.onClick.AddListener(Back);
    }
    public void Hide()
    {
        rectTransform.DOAnchorPosX(1500, 0.5f).OnComplete(()=>
        {
            gameObject.SetActive(false);
        });
    }
    public void AddRankingItem(string name, int rank)
    {
        if (uiRankItems==null)
        {
            uiRankItems = new List<UIRankItem>();
        }

        uiRankItems.Add(Instantiate(rankItem,transform.position,Quaternion.identity).GetComponent<UIRankItem>());
        uiRankItems[^1].gameObject.transform.SetParent(content);
        uiRankItems[^1].Setup(name, rank);
    }
    public void SortItem()
    {
        for(int i=0; i<uiRankItems.Count-1; i++)
        {
            for (int j=i+1; j<uiRankItems.Count; j++)
            {
                if (uiRankItems[i].rank > uiRankItems[j].rank)
                {
                    var t = uiRankItems[i];
                    uiRankItems[i] = uiRankItems[j];
                    uiRankItems[j] = t;
                }
            }
        }
        
    }
    public void SetupRankItemList()
    {
        foreach(var item in uiRankItems)
        {
            item.Setup();
        }    
    }
    void Back()
    {
        UIController.Instance.BackToMenu();
    }
}

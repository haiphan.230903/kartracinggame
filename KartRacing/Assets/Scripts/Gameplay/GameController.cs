using Gameplay;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[Serializable]
public class GameplayData
{
    public List<KartController> kartControllers;
    public int maxLap;
    public GameState gameState;
}
public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    public GameplayData gameplayData;
    [SerializeField] GameObject playerPrefab;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Debug.Log("Nickname = " + PhotonNetwork.NickName);

        Setup();
        StartCount();
    }
    void Setup()
    {
        for (int i=0; i<RoomDataManager.Instance.roomData.playerDatas.Count; i++)
        {
            PlayerData playerData = RoomDataManager.Instance.roomData.playerDatas[i];
            if (playerData.isCPU == false)
            {
                gameplayData.kartControllers[i].Setup(playerData.id, playerData.playerName, playerData.isCPU);
                gameplayData.kartControllers[i].gameObject.SetActive(false);
                if (gameplayData.kartControllers[i].kartInfo.isMine == false)
                {
                    continue;
                }
                KartController kartController = PhotonNetwork.Instantiate(playerPrefab.name, gameplayData.kartControllers[i].transform.position, Quaternion.identity, 0).GetComponent<KartController>();
                kartController.Setup(playerData.id, playerData.playerName, playerData.isCPU);
                gameplayData.kartControllers[i] = kartController;
            }
            else
            {
                gameplayData.kartControllers[i].Setup(playerData.id, playerData.playerName, playerData.isCPU);
            }
        }
    }    
    public int GetRank(KartInfo kartInfo)
    {
        int targetRank = 1;
        foreach (KartController kartController in gameplayData.kartControllers)
        {
            if (kartController.kartInfo.id != kartInfo.id)
            {
                if (CompareRank(kartInfo, kartController.kartInfo))
                {
                    targetRank++;
                }
            }
        }

        return targetRank;
    }
    public bool CompareRank(KartInfo kartInfo1, KartInfo kartInfo2)
    {
        float posCal1 = Vector3.Distance(kartInfo1.GetPosition(), kartInfo1.lastCheckPointPos);
        float posCal2 = Vector3.Distance(kartInfo2.GetPosition(), kartInfo2.lastCheckPointPos);
        //float rotCal1 = kart HARD, I'M CONFUSE AT VECTOR MATH!
        return kartInfo1.lap * 10000 + kartInfo1.lastCheckPointValue * 10 + posCal1< kartInfo2.lap * 10000 + kartInfo2.lastCheckPointValue * 10 + posCal2;
    }
    public void Goal()
    {
        Debug.Log("GOAL");
        //Camera.main.transform.SetParent(null);
        foreach (KartController kartController in gameplayData.kartControllers)
        {
            if (kartController.kartInfo.isMine)
            {
                kartController.RotateCameraToRank();
                break;
            }
        }
        FinishGame();
        if (CheckAllFinish())
        {
            StartCoroutine(Cor_Ranking());
        }
    }
    public void StartCount()
    {
        gameplayData.gameState = GameState.START;
        UIController.Instance.StartCount();
    }
    public void RunGame()
    {
        gameplayData.gameState = GameState.RUNNING;
    }
    public void FinishGame()
    {
        gameplayData.gameState = GameState.FINISH;
    }
    IEnumerator Cor_Ranking()
    {
        yield return new WaitForSeconds(3);
        Ranking();
    }
    void Ranking()
    {
        gameplayData.gameState = GameState.RANKING;
        foreach (KartController kartController in gameplayData.kartControllers)
        {
            if (kartController.kartInfo.finishRank == 0)
            {
                kartController.kartInfo.finishRank = kartController.kartInfo.rank;
            }
        }
        UIController.Instance.ShowRankingPanel();
    }
    bool CheckAllFinish()
    {
        foreach(KartController kartController in gameplayData.kartControllers)
        {
            //Debug.Log(kartController.kartInfo.name + " " + kartController.kartInfo.lap + " " + kartController.kartInfo.isCPU);
            if (kartController.kartInfo.lap <= gameplayData.maxLap && kartController.kartInfo.isCPU == false) return false;
        }
        return true;
    }
    public void BackToMenu()
    {
        Debug.Log("Back to menu");
    }
}

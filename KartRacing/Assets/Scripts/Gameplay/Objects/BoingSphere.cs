using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoingSphere : MonoBehaviour
{
    [SerializeField] LayerMask layermask;
    [SerializeField] float jumpVelo;

    [SerializeField,ReadOnly] float defaultSize;
    [SerializeField,ReadOnly] bool isAnimating = false;

    [Button]
    private void SetDefaultSize()
    {
        defaultSize = transform.localScale.x;
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((layermask & (1 << other.gameObject.layer)) != 0)
        {
            BoingAnim();
            other.transform.parent.GetComponent<KartController>().movement.Jump(jumpVelo);
        }
    }
    void BoingAnim()
    {
        if (isAnimating) return;
        isAnimating = true;
        transform.DOScale(defaultSize / 1.4f, 0.2f).OnComplete(() =>
        {
            transform.DOScale(defaultSize, 0.2f).OnComplete(() => isAnimating = false);
        });
    }
}

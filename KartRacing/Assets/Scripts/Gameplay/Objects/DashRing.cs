using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashRing : MonoBehaviour
{
    [SerializeField] LayerMask layermask;

    private void OnTriggerEnter(Collider other)
    {
        if ((layermask & (1 << other.gameObject.layer)) != 0)
        {
            KartController kartController = other.transform.parent.GetComponent<KartController>();
            if (kartController != null )
            {
                kartController.movement.Fire();
            }
            //other.transform.parent.GetComponent<KartController>().movement.Fire();
        }
    }
}

using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class KartAnimation : MonoBehaviour
{
    [SerializeField] KartController kartController;
    [SerializeField] Animator wheelAnim, playerAnim;
    [SerializeField] GameObject driftFullVfx;
    [SerializeField] TrailRenderer trailLeftVfx, trailRightVfx, fireVfx;
    [SerializeField] ParticleSystem speedUpVfx, landedVfx, appearVfx;

    [SerializeField] Transform kartTrans, playerTrans, wheelsTrans, effectsTrans;

    [SerializeField] float trailVfxTime = 2;
    [SerializeField] float fireVfxTime = 1;

    [SerializeField,ReadOnly] int driftSteer;
    public void Steer(float steering)
    {
        wheelAnim.SetFloat("Steering", steering);
        playerAnim.SetFloat("Steering", steering);
    }
    public void Move(float speed)
    {
        wheelAnim.SetFloat("Speed", speed);
    }
    public void StartDrift(float driftAxis)
    {
        if (driftAxis<-0.1f && driftSteer != 1)
        {
            RotateDriftStuff(-25);
            driftSteer = -1;
        }
        else if (driftAxis>0.1f && driftSteer != -1)
        {
            RotateDriftStuff(25);
            driftSteer = 1;
        }

        DOTween.Kill(trailLeftVfx);
        DOTween.Kill(trailRightVfx);
        trailLeftVfx.time = trailVfxTime;
        trailRightVfx.time = trailVfxTime;
    }
    void RotateDriftStuff(float rotateValue)
    {
        DOTween.Kill(kartTrans);
        DOTween.Kill(playerTrans);
        DOTween.Kill(wheelsTrans);
        DOTween.Kill(effectsTrans);
        kartTrans.DOLocalRotate(new Vector3(0, rotateValue, 0), 0.3f);
        playerTrans.DOLocalRotate(new Vector3(0, rotateValue, 0), 0.3f);
        wheelsTrans.DOLocalRotate(new Vector3(0, rotateValue, 0), 0.3f);
        effectsTrans.DOLocalRotate(new Vector3(0, rotateValue, 0), 0.3f);
    }
    public void DriftFull()
    {
        driftFullVfx.SetActive(true);
    }
    public void EndDriftFull()
    {
        driftFullVfx.SetActive(false);
    }
    public void EndDrift()
    {
        RotateDriftStuff(0);
        driftSteer = 0;
        trailLeftVfx.DOTime(0, 1f).SetEase(Ease.InExpo);
        trailRightVfx.DOTime(0, 1f).SetEase(Ease.InExpo);
        EndDriftFull();
    }
    public void Fire()
    {
        if (speedUpVfx != null)
        {
            speedUpVfx.Play();
        }
        DOTween.Kill(fireVfx);
        fireVfx.time = fireVfxTime;

        if (kartController.kartInfo.isMine)
        {
            PostProcessController.Instance.StartMotionBlur();
        }
    }
    public void EndFire()
    {
        fireVfx.DOTime(0, 0.5f);
    }

    public void Landed()
    {
        landedVfx.Play();
    }

    public void Jump(bool isGrounded)
    {
        playerAnim.SetBool("Grounded", isGrounded);
    }    
    public void Disappear()
    {
        appearVfx.Play();
        kartTrans.gameObject.SetActive(false);
        playerTrans.gameObject.SetActive(false);
        wheelsTrans.gameObject.SetActive(false);
    }
    public void Appear()
    {
        appearVfx.Play();
        kartTrans.gameObject.SetActive(true);
        playerTrans.gameObject.SetActive(true);
        wheelsTrans.gameObject.SetActive(true);
    }
}

using DG.Tweening;
using NaughtyAttributes;
using NaughtyAttributes.Test;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] KartController kartController;
    [ReadOnly, SerializeField] float charge, drift;

    [ReadOnly] public float speed = 0, turnSpeed;
    [SerializeField] bool isDriftLeft, isCanFire = false, isFiring = false, isEndFiring = false, isMoveBack, isCanCharge = true;
    
    [SerializeField] Rigidbody rb;

    Coroutine fireCor, endFireCor;

    public void Charge(bool isGrounded)
    {
        if (isCanCharge == false) return;
        SOKart soKart = kartController.kartInfo.soKart;

        isMoveBack = false;
        if (isGrounded)
        {
            charge = math.lerp(charge, soKart.maxCharge, soKart.chargeSpeed * Time.deltaTime);
        }
        else
        {
            charge = math.lerp(charge, soKart.maxCharge/3f, soKart.chargeSpeed * Time.deltaTime);
        }
    }
    public void UnCharge()
    {
        isMoveBack = false;
        charge = math.lerp(charge, 0, kartController.kartInfo.soKart.unchargeSpeed * Time.deltaTime);
    }
    public void StopCharge()
    {
        if (isMoveBack) return;
        Debug.Log("Stop charge");
        charge = 0;
        isFiring = false;
        isEndFiring = false;
        if (fireCor != null)
        {
            StopCoroutine(fireCor);
        }
        if (endFireCor != null)
        {
            StopCoroutine(endFireCor);
        }
    }
    public void Back(bool isGrounded)
    {
        SOKart soKart = kartController.kartInfo.soKart;
        isMoveBack = true;
        if (isGrounded)
        {
            charge = math.lerp(charge, -soKart.maxCharge / 2f, soKart.chargeSpeed * Time.deltaTime);
        }
        else
        {
            charge = math.lerp(charge, -soKart.maxCharge / 3.5f, soKart.chargeSpeed * Time.deltaTime);
        }
    }
    public void Move()
    {
        SOKart soKart = kartController.kartInfo.soKart;
        if (isFiring == false && isEndFiring == false)
        {
            speed = charge * soKart.chargeFactor;
        }
        else if (isEndFiring == false)
        {
            speed = soKart.maxCharge * soKart.fireBootsValue * soKart.chargeFactor;
        }    
        //transform.Translate(new Vector3(0, 0, speed*Time.deltaTime));
        Vector3 dir = transform.forward ;
        dir.y = 0;
        dir = dir * speed * Time.deltaTime;
        rb.velocity = new Vector3(dir.x, rb.velocity.y, dir.z);
    }
    public void Steer(float steerAxis)
    {
        SOKart soKart = kartController.kartInfo.soKart;
        turnSpeed = steerAxis * soKart.turnFactor * (speed / (soKart.maxCharge * soKart.chargeFactor)); //maxCharge * chargeFactor = maxSpeed
        transform.Rotate(new Vector3(0, turnSpeed * Time.deltaTime, 0));        
        if (transform.rotation.eulerAngles.y<0)
        {
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + 360, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        }
    }
    public void Drift(float driftAxis, bool isDriftLeft)
    {
        SOKart soKart = kartController.kartInfo.soKart;
        kartController.kartAnimation.StartDrift(driftAxis);

        if (drift == 0)
        {
            this.isDriftLeft = isDriftLeft;
            ResetDrift();
        }
        else if (isDriftLeft != this.isDriftLeft)
        {
            drift -= 0.5f;
            if (drift < 0) drift = 0;
            driftAxis = -driftAxis;
            //Debug.Log("Drift nguoc");
        }

        if (drift > soKart.fireDriftValue)
        {
            isCanFire = true;
            kartController.kartAnimation.DriftFull();
        }
        else
        {
            isCanFire = false;
            kartController.kartAnimation.EndDriftFull();
        }

        float driftSpeed = driftAxis * drift * soKart.driftFactor * (speed / (soKart.maxCharge * soKart.chargeFactor)); //maxCharge * chargeFactor = maxSpeed
        transform.Rotate(new Vector3(0, driftSpeed * Time.deltaTime, 0));
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

        if (driftAxis < 0.1f && driftAxis > -0.1f || isDriftLeft != this.isDriftLeft) return;

        drift++;
        if (drift > soKart.maxDrift)
        {
            drift = soKart.maxDrift;
        }
        if (drift > soKart.fireDriftValue)
        {
            isCanFire = true;
            kartController.kartAnimation.DriftFull();
        }
        else
        {
            isCanFire = false;
            kartController.kartAnimation.EndDriftFull();
        }
    }
    public void EndDrift()
    {
        if (isCanFire)
        {
            Fire();
            isCanFire = false;
        }

        ResetDrift();
    }
    public void ResetDrift()
    {
        drift = 0;
        kartController.kartAnimation.EndDrift();
    }
    public void Fire()
    {
        isFiring = true;
        isEndFiring = false;
        if (fireCor != null)
        {
            StopCoroutine(fireCor);
        }
        if (endFireCor != null)
        {
            StopCoroutine(endFireCor);
        }
        kartController.kartAnimation.Fire();
        fireCor = StartCoroutine(Cor_Firing());
    }    
    IEnumerator Cor_Firing()
    {
        SOKart soKart = kartController.kartInfo.soKart;
        yield return new WaitForSeconds(soKart.fireTime);
        isFiring = false;
        kartController.kartAnimation.EndFire();
        if (endFireCor!=null)
        {
            StopCoroutine(endFireCor);
        }
        endFireCor = StartCoroutine(Cor_EndFiring());
    }
    IEnumerator Cor_EndFiring()
    {
        SOKart soKart = kartController.kartInfo.soKart;
        isEndFiring = true;
        while (Mathf.Abs(speed - charge * soKart.chargeFactor) > 50)
        {
            speed = Mathf.Lerp(speed, charge * soKart.chargeFactor, 0.01f);
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForFixedUpdate();
        isEndFiring = false;
    }    

    public void RotateKartTo(Vector3 rotateTarget)
    {
        isCanCharge = false;
        ReduceSpeed();
        Vector3 curRotation = new Vector3(0,(int)transform.rotation.eulerAngles.y%360,0);
        rotateTarget.y = (int)((rotateTarget.y+360) % 360);
        transform.DORotate((3 * rotateTarget + curRotation) / 4, 0.3f).SetEase(Ease.Linear).OnComplete(() => isCanCharge = true);
    }
    public void Jump(float jumpVelo)
    {
        rb.velocity += new Vector3(0, jumpVelo, 0);
        ReduceSpeed();
    }

    public void ReduceSpeed()
    {
        charge /= 6;
    }
}

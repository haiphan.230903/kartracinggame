using DG.Tweening;
using NaughtyAttributes;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
public class KartInfo
{
    [SerializeField] KartController kartController;
    public SOKart soKart;
    public int id;
    public string name;
    [ReadOnly] public int lastCheckPointValue, lap, rank, finishRank;
    public bool isMine, isCPU;
    [ReadOnly] public Vector3 lastCheckPointPos, lastCheckPointRot;

    public void Setup(int id, string name, bool isCPU)
    {
        this.id = id;
        this.name = name;
        this.isCPU = isCPU;
    }
    public void SetCheckPointValue(int value, Vector3 checkPointPos, Vector3 checkPointRot)
    {
        if (GameController.Instance.gameplayData.gameState != GameState.RUNNING) return;
        bool isReverse = false;
        if (lastCheckPointValue > value)
        {
            isReverse = true;
        }
        if (value - lastCheckPointValue > 150) //500 la khoang cach de xac dinh di nguoc hay qua lap moi khi qua cong
        {
            //day la truong hop di nguoc qua cong va toi checkpoint gan do
            //lastCheckPointValue trong truong hoo nay sco le = 0     
            lap--;
            isReverse = true;
        }
        if (value == 0 && lastCheckPointValue > 150) //di qua gate
        {
            isReverse = false;
            lap++;
            if (isMine)
            {
                UIController.Instance.UpdateLap(lap);
                if (lap > GameController.Instance.gameplayData.maxLap)
                {
                    isCPU = true;
                    finishRank = rank;
                }
            }
        }
        lastCheckPointValue = value;
        lastCheckPointRot = checkPointRot;
        lastCheckPointPos = checkPointPos;

        if (isMine)
        {
            if (isReverse)
            {
                UIController.Instance.AnnounceReverse(true);
            }
            else
            {
                UIController.Instance.AnnounceReverse(false);
            }
        }
    }
    public void CalculateRank()
    {
        rank = GameController.Instance.GetRank(this);
        if (isMine)
        {
            UIController.Instance.UpdateRank(rank);
        }
    }
    public Vector3 GetPosition()
    {
        return kartController.transform.position;
    }
}

public class KartController : MonoBehaviour
{
    public Movement movement;
    public KartAnimation kartAnimation;
    public KartInfo kartInfo;
    [SerializeField] UIName uiName;

    [SerializeField, ReadOnly] float steerAxis;
    
    //[SerializeField, ReadOnly] Vector3 checkPointRotation;

    [SerializeField] LayerMask groundLayer, obstacleLayer, terrainLayer;
    public bool isGrounded;
    [SerializeField,ReadOnly] bool isTouchOutside, isStartTouchOutside;
    [SerializeField, ReadOnly, ShowIf("isCPU")] bool isEndMoveBack;
    [SerializeField, ReadOnly, ShowIf("isCPU")] float leftDis, rightDis, forwardDis;
    Coroutine endMoveBackCor, touchOutsideCor;
    public Transform camPlayPos, camRankPos;

    //[SerializeField] PhotonView ptView;

    IEnumerator Cor_InitData()
    {
        yield return new WaitUntil(()=>UIController.Instance != null);
        isEndMoveBack = true;
        kartInfo.lap = 1;

        if (kartInfo.isMine)
        {
            UIController.Instance.UpdateLap(kartInfo.lap, false);
            Camera.main.transform.SetParent(this.transform);
            Camera.main.transform.position = camPlayPos.transform.position;
        }
    }
    public void Setup(int id, string name, bool isCPU)
    {
        kartInfo.Setup(id, name, isCPU);

        if (kartInfo.id == RoomDataManager.Instance.playerData.id)
        {
            kartInfo.isMine = true;
        }
        else
        {
            kartInfo.isMine = false;
        }

        uiName.Setup(name, kartInfo.isMine);

        StartCoroutine(Cor_InitData());
    }
    private void Update()
    {
        if (kartInfo.isCPU == false)
        {
            if (GameController.Instance.gameplayData.gameState == GameState.RUNNING && isTouchOutside == false)
            {
                PlayerInput();
            }
        }
        else
        {
            if ((int)GameController.Instance.gameplayData.gameState >= (int)GameState.RUNNING && isTouchOutside == false)
            {
                CPUInput();
            }
        }

        Collider[] colls = Physics.OverlapBox(transform.position + new Vector3(0, -0.02f, 0), new Vector3(1f, 0.2f, 1.5f), quaternion.identity, groundLayer);
        bool isGroundedNew = (colls != null && colls.Length > 0);
        if (isGrounded == false && isGroundedNew)
        {
            kartAnimation.Landed();
        }
        isGrounded = isGroundedNew;

        foreach (var coll in colls)
        {
            if ((terrainLayer & (1 << coll.gameObject.layer)) != 0)
            {
                TouchOutsideTrack();
            }
        }

        kartAnimation.Jump(isGrounded);
        kartAnimation.Steer(steerAxis);
        kartAnimation.Move(movement.speed);

        kartInfo.CalculateRank(); //change rank value
        
    }
    private void FixedUpdate()
    {
        movement.Move();
    }

    void PlayerInput()
    {
        if (kartInfo.isMine == false) return;

        if (Input.GetKey(KeyCode.A))
        {
            steerAxis = math.lerp(steerAxis, -1, 0.1f);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            steerAxis = math.lerp(steerAxis, 1, 0.1f);
        }
        else
        {
            steerAxis = math.lerp(steerAxis, 0, 0.1f);
        }

        //if (isGrounded)
        {
            if (Input.GetKey(KeyCode.W))
            {
                movement.Charge(isGrounded);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                movement.Back(isGrounded);
            }
            else
            {
                movement.UnCharge();
            }

            if (Input.GetKey(KeyCode.M))
            {
                movement.Drift(steerAxis, steerAxis < 0);
            }
            else
            {
                movement.Steer(steerAxis);
            }

            if (Input.GetKeyUp(KeyCode.M))
            {
                movement.EndDrift();
            }
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            movement.Fire();
        }
    }

    void CPUInput()
    {
        forwardDis = RaycastDistance(0);
        rightDis = RaycastDistance(30);
        leftDis = RaycastDistance(-30);

        if (forwardDis > 3 && isEndMoveBack == true)
        {
            movement.Charge(isGrounded);
        }
        else /*if (forwardDis < 0.5f)*/
        {
            isEndMoveBack = false;
            movement.Back(isGrounded);
            endMoveBackCor = StartCoroutine(Cor_EndMoveBack(3));
        }
        //else
        //{
        //    movement.UnCharge();
        //}

        if ((leftDis - rightDis > 3 && isEndMoveBack == true) || (rightDis - leftDis > 3 && isEndMoveBack == false))
        {
            steerAxis = math.lerp(steerAxis, -1, 0.1f);
        }
        else if ((rightDis - leftDis > 3 && isEndMoveBack == true) || (leftDis - rightDis > 3 && isEndMoveBack == false))
        {
            steerAxis = math.lerp(steerAxis, 1, 0.1f);
        }
        else
        {
            steerAxis = math.lerp(steerAxis, 0, 0.1f);
        }

        movement.Steer(steerAxis);
    }
    IEnumerator Cor_EndMoveBack(float sec)
    {
        yield return new WaitForSeconds(sec);
        isEndMoveBack = true;
    }
    float RaycastDistance(float angle)
    {
        RaycastHit hit;
        Vector3 direction = Quaternion.Euler(0, angle, 0) * transform.forward;

        float distanceToHit = 100;
        if (Physics.Raycast(transform.position, direction, out hit, 10f, obstacleLayer))
        {
            distanceToHit = hit.distance;
            Debug.DrawLine(transform.position, hit.point, Color.green);
            //Debug.Log("Hit something to the right! Distance: " + distanceToHit);
        }
        return distanceToHit;
    }
    public void SetCheckPointValue(int value, Vector3 checkPointPosition, Vector3 checkPointRotation)
    {
        kartInfo.SetCheckPointValue(value, checkPointPosition, checkPointRotation);
        //this.checkPointRotation = checkPointRotation;
    }
    public void RotateKartWhenHit()
    {
        Vector3 rotateTarget = kartInfo.lastCheckPointRot;
        int gapRotate = Mathf.Abs((int)transform.rotation.eulerAngles.y % 360 - (int)kartInfo.lastCheckPointRot.y % 360);
        if (gapRotate > 90)
        {
            rotateTarget.y +=180;
            movement.RotateKartTo(rotateTarget);
        }
        //else if (gapRotate > 80)
        //{
        //    movement.StopCharge();
        //}
        else
        {
            movement.RotateKartTo(rotateTarget);
        }
    }

    public void RotateCameraToRank()
    {
        Camera.main.transform.DOLocalMove(camRankPos.localPosition, 2);
        Camera.main.transform.DOLocalRotate(camRankPos.localRotation.eulerAngles, 2);
    }

    void TouchOutsideTrack()
    {
        if (isStartTouchOutside) return;
        isStartTouchOutside = true;
        Debug.Log("Touch");
        if (touchOutsideCor != null)
        {
            StopCoroutine(touchOutsideCor);
        }
        touchOutsideCor = StartCoroutine(Cor_TouchOutsideTrack());
    }
    IEnumerator Cor_TouchOutsideTrack()
    {
        yield return new WaitForSeconds(0.5f);
        Collider[] colls = Physics.OverlapBox(transform.position + new Vector3(0, -0.02f, 0), new Vector3(1f, 0.2f, 1.5f), quaternion.identity, terrainLayer);
        if (colls != null && colls.Length > 0)
        {
            isTouchOutside = true;
            yield return new WaitForSeconds(1);
            kartAnimation.Disappear();
            yield return new WaitForSeconds(1);
            transform.DOMove(kartInfo.lastCheckPointPos, 1);
            transform.DORotate(kartInfo.lastCheckPointRot, 1);
            yield return new WaitForSeconds(1.1f);
            kartAnimation.Appear();
            isTouchOutside = false;
        }
        isStartTouchOutside = false;
    }
    //public float a = 0, b = -0.5f, c = 0f;
    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawCube(transform.position + new Vector3(a, b, c), new Vector3(1f, 0.2f, 1.5f));
    //}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollToward : MonoBehaviour
{
    [SerializeField] LayerMask layermask;
    [SerializeField] KartController kartController;

    private void OnTriggerEnter(Collider other)
    {
        if ((layermask & (1 << other.gameObject.layer)) != 0) 
        {
            kartController.RotateKartWhenHit();
        }
    }
}

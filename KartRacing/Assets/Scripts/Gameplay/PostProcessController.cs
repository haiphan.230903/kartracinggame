using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessController : MonoBehaviour
{
    public static PostProcessController Instance { get; private set; }
    [SerializeField] Volume volume;
    MotionBlur motionBlur;
    [SerializeField] float motionBlurIntensity;
    Coroutine motionBlurCor;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        volume.profile.TryGet(out motionBlur);
    }

    public void StartMotionBlur()
    {
        motionBlur.intensity.value = motionBlurIntensity;
        if(motionBlurCor!= null)
        {
            StopCoroutine(motionBlurCor);
        }
        motionBlurCor = StartCoroutine(Cor_MotionBlue());
    }
    IEnumerator Cor_MotionBlue()
    {
        while(motionBlur.intensity.value>0)
        {
            motionBlur.intensity.value -= 0.025f;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }
}

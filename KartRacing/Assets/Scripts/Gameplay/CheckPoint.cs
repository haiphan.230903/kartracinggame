using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] int value;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent.GetComponent<KartController>().SetCheckPointValue(value, transform.position, transform.rotation.eulerAngles);
        }
    }
    public void SetValue(int value)
    {
        this.value = value;
    }
    [Button]
    void SetValue()
    {
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            transform.parent.GetChild(i).GetComponent<CheckPoint>().SetValue(i*10);
        }
    }
}

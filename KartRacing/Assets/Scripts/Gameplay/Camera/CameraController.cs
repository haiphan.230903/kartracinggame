using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance { get; private set; }
        public float moveSpeed, rotateSpeed;

        [SerializeField] Transform targetTrans;
        //public bool isFollowPlayer, isNotCheckBound;
        //bool isScoll = false;

        Vector3 target;
        // Start is called before the first frame update
        void Awake()
        {
            Instance = this;
            //anim = GetComponent<Animator>();
            //rigi = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            //if (isMoveToNewBound == false)
            //{
            //    if (Player.Instance != null && isFollowPlayer)
            //    {
            //        // Tao camera di chuyen theo targetObj
            //        target = Player.Instance.transform.position;
            //        target.z -= 12;

            //        if (target.x < minX) target.x = minX;
            //        if (target.x >= maxX) target.x = maxX;
            //        if (target.y < minY) target.y = minY;
            //        if (target.y >= maxY) target.y = maxY;

            //        transform.position = Vector3.Lerp(transform.position, target, moveSpeed);
            //    }
            //    else if (isFollowPlayer == false)
            //    {
            //        CheckPos();
            //    }
            //}
            MoveToTarget();
        }
        void MoveToTarget()
        {
            //transform.position = Vector3.Lerp(transform.position, targetTrans.position, moveSpeed);
            //transform.rotation = Quaternion.Euler(Vector3.Lerp(transform.rotation.eulerAngles, targetTrans.rotation.eulerAngles, rotateSpeed));
            transform.position = targetTrans.position;
            transform.rotation = targetTrans.rotation;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/SO Kart", fileName = "SO Kart")]
public class SOKart : ScriptableObject
{
    [Header("Charge")]
    public float maxCharge;
    public float chargeFactor, unchargeSpeed, chargeSpeed; 

    [Header("Turn")]
    public float turnFactor;

    [Header("Drift")]
    public float maxDrift;
    public float driftFactor;

    [Header("Fire")]
    public float fireDriftValue;
    public float fireTime, fireBootsValue;
}
